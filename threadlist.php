<?php

  require('inc/acc_db.php');
  require('inc/infos.php');

?>
    <!DOCTYPE html>
    <html>

    <head>
        <title>
            <?php echo("Catégorie K-net - ".$boardinfo['nom']); ?>
        </title>
        <?php require('inc/jslibs.php'); require('inc/csslibs.php'); ?>
        <script>
            $.material.init();

        </script>
    </head>

    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <div class="navbar navbar-inverse">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
                                <a class="navbar-brand" href="javascript:void(0)">LaFibre.info</a>
                            </div>
                            <div class="navbar-collapse collapse navbar-inverse-collapse">
                                <ul class="nav navbar-nav">
                                    <li><a href="javascript:void(0)">Derniers messages non-lus</a></li>
                                    <li><a href="javascript:void(0)">Messages suivis non-lus</a></li>

                                </ul>
                                <form class="navbar-form navbar-left">
                                    <div class="form-group">
                                        <input type="text" class="form-control col-sm-8" placeholder="Recherche">
                                    </div>
                                </form>
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown">
                                        <a class="dropdown-toggle" data-toggle="dropdown">Compte
            <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="javascript:void(0)">Connexion</a></li>
                                            <li><a href="javascript:void(0)">Inscription</a></li>
                                            <li class="divider"></li>
                                            <li><a href="javascript:void(0)">Mot de passe oublié</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="jumbotron">
                                <img class="fai-offer-image" src="https://www.k-net.fr/sites/all/themes/bootstrap/img/offres/bloc-offres-3S.jpg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <ul class="nav nav-pills nav-stacked" style="max-width: 300px;">
                        <li class="active" style="background-color:light-green;color:black;"><a href="javascript:void(0)">Actualités K-Net</a></li>
                        <li class="" style="background-color:green;"><a href="javascript:void(0)">Espace technique Internet</a></li>
                        <li class="" style="background-color:dark-red;"><a href="javascript:void(0)">Espace technique Télévision</a></li>
                        <li class="" style="background-color:dark-blue;"><a href="javascript:void(0)">Weathermap AS24904</a></li>
                    </ul>
                </div>
                <div class="col-md-9">
                    <table class="table table-striped" table-condensed>
                        <tbody>
                            <tr>
                                <td>
                                    <span class="label label-success">Actualité K-Net</span>
                                </td>
                                <td>
                                    Nouvelles offres K-net
                                </td>
                                <td>
                                    01/01/2019
                                </td>
                                <td>
                                    Dernier message: K-net (02/01/2019)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="label label-warning">Espace technique Internet</span>
                                </td>
                                <td>
                                    Panne lien SIEA.
                                </td>
                                <td>
                                    29/12/2018
                                </td>
                                <td>
                                    Dernier message: jack (30/12/2018)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="label label-info">Espace technique Télévision</span>
                                </td>
                                <td>
                                    Panne TV
                                </td>
                                <td>
                                    5/10/2018
                                </td>
                                <td>
                                    Dernier message: Damien (2/11/2018)
                                </td>
                            </tr>
                            <tr class="active">

                                <tr class="success">

                                    <tr class="warning">

                                        <tr class="danger">

                                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <p class="text-muted text-center">
                <small>LaFibreBB - ... // Le logo K-net et l'image d'offre apartiennent à la société K-net. FAI choisi à titre d'exemple.</small>
            </p>
        </div>
        </div>
        </div>
    </body>

    </html>
