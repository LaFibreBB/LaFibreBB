# LaFibre
Créer un ***Bulletin Board*** adapté au forum [LaFibre.info](https://lafibre.info)

# To-do list :
- Faire le design (en cours de réalisation par Troniq, puis demande d'avis)
- Partie technique (étudier la structure d'SMF pour migrer ensuite)

# Bibliothèques utilisées :
- jQuery
- PrefixFree (+ son module jQuery)
- Bootstrap + thème "Paper"




